#!/usr/bin/env bash

#Configuration path which was set in the jenkins
#echo "Configuration directory set in jenkins = " ${CONFIG_DIR}

#Configurations repository directory
APP_DIR=${JENKINS_HOME}/workspace/${JOB_NAME}/
echo ${APP_DIR}

#Copy the right config file for specific environment
#cp ${CURR_DIR}/${CONFIG_DIR}/config/config.json  ${APP_TEST_SCRIPTS_DIR}/Truman/config.json
#echo "Copying files from  " ${CURR_DIR}/${CONFIG_DIR}/config/config.json " to " ${APP_TEST_SCRIPTS_DIR}/Truman/config.json "\n"

#echo "====================Starting Test cases============"
#(cd ${APP_DIR} && xcodebuild -workspace brothers4life.xcworkspace/ -scheme brothers4life -destination 'platform=iOS Simulator,name=iPhone 8 Plus,OS=11.2' )

echo "====================Starting Beta Deployment============"
(cd ${APP_DIR} && chmod -R 777 * )
(cd ${APP_DIR} && fastlane beta )

#echo "====================End of Test cases============"
